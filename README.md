# Shadowsocks Setup

## Refrences

[shadowsocks-manager](https://github.com/shadowsocks/shadowsocks-manager)

[shadowsocks-demo](https://app.3840x2160.work/home/index)

## Usage

1. update with your ip at 5 line in `.ssmgr/webgui.yml`

2. build, `docker build ./ -t ssmgr:latest`

3. run docker, `docker run --restart always --net=host -itd ssmgr:latest /bin/bash`

4. go into docker container, `docker exec -it {containerId} bash`

5. visit http://your-server-ip, to manage

## PS

mark sure 3000 of TCP, and 50000 - 50010 of UDP are available in your server, 50000 - 50010 are used for vpn account.

## Helps

1. To delete all containers

    docker rm -vf $(docker ps -a -q)

2. To delete all the images

    docker rmi -f $(docker images -a -q)

3. show listened port

    lsof -i -P -n | grep LISTEN

4. view ufw status and allow ports

    ufw status / sudo ufw allow 5000

## Summary

1. docker run --net=host 无法与 -p 一起使用，host，容器网络不会与主机的隔离（容器共享主机的网络），
   并且容器不会获取自己的ip地址。例如，如果运行一个绑定到端口80的容器并使用主机网络，
   则该容器的应用程序在主机IP地址的端口80上可用。
    [https://docs.docker.com/network/host/](https://docs.docker.com/network/host/)
