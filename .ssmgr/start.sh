#!/bin/bash

# 启动 redis
/etc/init.d/redis-server restart

# start shadowsocks
# PS: forever 启动时最后必须指定一个文件
forever start -c "ss-manager -m aes-256-cfb -u --manager-address 127.0.0.1:4000" ~/.ssmgr/noop

# start ssmgr
forever start -c "ssmgr -c" ~/.ssmgr/default.yml

# start ssmgr-webgui
forever start -c "ssmgr -c" ~/.ssmgr/webgui.yml

# Extra line added in the script to run all command line arguments
exec "$@";
